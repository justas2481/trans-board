<?php
require_once 'TransportData.php';

/**
 * Fetches the data from Vilnius Transport.
 */
class GPSDataFetcher
{
    private const ENDPOINT_URL = 'http://www.stops.lt/vilnius/gps_full.txt';

    public function __construct()
    {
    }

    public function callAPI(): array
    {
        $data = explode(
            "\n",
            str_replace(",\n", "\n", $this->request(self::ENDPOINT_URL))
        );
        if (!is_array($data)) {
            return [];
        }

        return $data;
    }

    private function request(string $url): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public function findByNumberAndType(string $type, string $number, array $transportTable): array
    {
        $data = [];
        foreach ($transportTable as $transport) {
            if ($type === $transport->getType() && $number === $transport->getNumber()) {
                $data[] = $transport;
            }
        }

        return $data;
    }

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * Taken from: https://stackoverflow.com/questions/10053358/measuring-the-distance-between-two-coordinates-in-php
     * 
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    public static function vincentyGreatCircleDistance(
        float $latitudeFrom,
        float $longitudeFrom,
        float $latitudeTo,
        float $longitudeTo,
        int $earthRadius = 6371000
    ): float {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return $angle * $earthRadius;
    }

    /**
     * Borrowed from: https://stackoverflow.com/questions/2859867/relative-position-in-php-between-2-points-lat-long
     */
    public function bearing(float $lat1, float $lon1, float $lat2, float $lon2): int
    {
        //difference in longitudinal coordinates
        $dLon = deg2rad($lon2) - deg2rad($lon1);

        //difference in the phi of latitudinal coordinates
        $dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));

        //we need to recalculate $dLon if it is greater than pi
        if (abs($dLon) > pi()) {
            if ($dLon > 0) {
                $dLon = (2 * pi() - $dLon) * -1;
            } else {
                $dLon = 2 * pi() + $dLon;
            }
        }
        //return the angle, normalized
        return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
    }

    public function cardinalDirection(int $azimuth): string
    {
        $cardinals = ['šiaurė', 'šiaurės rytai', 'rytai', 'pietryčiai', 'pietūs', 'pietvakariai', 'vakarai', 'šiaurės vakarai'];
        $index = round((($azimuth %= 360) < 0 ? $azimuth + 360 : $azimuth) / 45) % 8;
        return $cardinals[$index];
    }
}
