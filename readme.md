# Real-time Vilnius transport data for the blind individuals

I have decided to experiment with real-time data provided by [stops.lt](https://www.stops.lt/vilnius/) just to see if it can help blind people to find ariving / departuring transport easier.

If this data proves to be useful and helpful, project will be completely overritten. It would be nice to let blind users to find relevant information by their current location and bus stop data which is provided by mentioned third party. More about this data that I rely on in this project can be found here: [https://github.com/vilnius/transportas](https://github.com/vilnius/transportas).