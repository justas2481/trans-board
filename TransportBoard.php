<?php
require_once 'GPSDataFetcher.php';
require_once 'TransportData.php';

$userLat = !empty($_GET['lat']) ? (float)$_GET['lat'] : 0.0;
$userLon = !empty($_GET['lon']) ? (float)$_GET['lon'] : 0.0;
$userTransportType = !empty($_GET['type']) ? (string)$_GET['type'] : '';
$userTransportNumber = !empty($_GET['num']) ? (string)$_GET['num'] : '';
$fetcher = new GPSDataFetcher();
$data = $fetcher->callAPI();
if (false == $data) {
    die('Unable to fetch the data.');
}

for ($i = 1; $i <= count($data); $i++) {
    $transportTable[$i] = new TransportData();
}

foreach ($data as $key => $transport) {
    if ($key == 0) {
        continue;
    }

    $transport = str_getcsv($transport, ',', '"', "\\");
    if (!is_array($transport) || count($transport) < 12) {
        break;
    }

    $transportTable[$key]->setType($transport[0]);
    $transportTable[$key]->setNumber($transport[1]);
    $transportTable[$key]->setTripNumber($transport[2]);
    $transportTable[$key]->setCarNumber($transport[3]);
    $transportTable[$key]->setLongitude($transport[4]);
    $transportTable[$key]->setLatitude($transport[5]);
    $transportTable[$key]->setSpeed($transport[6]);
    $transportTable[$key]->setAzimuth($transport[7]);
    $transportTable[$key]->setTripStartInMinutes($transport[8]);
    $transportTable[$key]->setDiviationInSeconds($transport[9]);
    $transportTable[$key]->setMeasurementTime($transport[10]);
    $transportTable[$key]->setCarType($transport[11]);
}

$availableTransport = $fetcher->findByNumberAndType($userTransportType, $userTransportNumber, $transportTable);
usort($availableTransport, function (TransportData $a, TransportData $b) use ($userLat, $userLon): int {
    return
        floor(
            GPSDataFetcher::vincentyGreatCircleDistance($userLat, $userLon, $a->getLatitude(), $a->getLongitude())
        )
        - floor(
            GPSDataFetcher::vincentyGreatCircleDistance($userLat, $userLon, $b->getLatitude(), $b->getLongitude())
        ) > 0
        ? 1 : -1;
});

$output = '
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Viešojo Vilniaus transporto realiuoju laiku paieškos rezultatai</title>
    <style>
    body {
        font-size: 45px;
    }
    </style>
</head>
<body>
<a href="/t/TransportBoard.php?type=' .
    urlencode($_GET['type']) .
    '&amp;num=' . urlencode($_GET['num']) .
    '&amp;lat=' . urlencode($_GET['lat']) .
    '&amp;lon=' . urlencode($_GET['lon']) .
    '">Atnaujinti duomenis</a>
';
foreach ($availableTransport as $transport) {
    $output .= '<h1>' . $transport->getType() . ', ' . $transport->getNumber() . ', ' .
        floor(GPSDataFetcher::vincentyGreatCircleDistance($userLat, $userLon, $transport->getLatitude(), $transport->getLongitude())) .
        ' m., ' . $transport->getSpeed() . ' km / val.</h1>';
    $output .= '<ul>';
    $output .= '<li>Garažinis nr.: ' . $transport->getCarNumber() . '</li>';
    $output .= '<li>Judėjimo kryptis: ' . $fetcher->cardinalDirection($transport->getAzimuth()) . ', ' .
        $transport->getAzimuth() . ' laipsn.</li>';
    $output .= '<li>Reliatyvi kryptis nuo transporto priemonės į vartotoją: ' . $fetcher->cardinalDirection($fetcher->bearing($transport->getLatitude(), $transport->getLongitude(), $userLat, $userLon)) . ', ' .
        $fetcher->bearing($transport->getLatitude(), $transport->getLongitude(), $userLat, $userLon) . ' laipsn. šiaurės atžvilgiu.</li>';
    $output .= '<li>Nuokrypis nuo grafiko: ' . $transport->getDiviationInSeconds() . ' s.</li>';
    $output .= '<li>Platuma; ilguma: ' . $transport->getLatitude() . '; ' . $transport->getLongitude() . '</li>';
    $output .= '<li>Kelionės nr.: ' . $transport->getTripNumber() . '</li>';
    $output .= '</ul>';
}
$output .= '
<footer>Duomenys apie transportą gaunami iš SĮ &quot;Susisiekimo paslaugos&quot; ir tada papildomai filtruojami.</footer>
</body>
</html>
';
echo $output;
