<?php

class TransportData
{
    private $type = '';
    private $number = '';
    private $tripNumber = '';
    private $carNumber = '';
    private $longitude = 0.0;
    private $latitude = 0.0;
    private $speed = 0;
    private $azimuth = 0;
    private $tripStartInMinutes = 0;
    private $diviationInSeconds = 0;
    private $measurementTime = 0;
    private $carType = '';

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function setTripNumber(string $tripNumber): self
    {
        $this->tripNumber = $tripNumber;

        return $this;
    }

    public function setCarNumber(string $carNumber): self
    {
        $this->carNumber = $carNumber;

        return $this;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $this->obtainLatOrLon($longitude);

        return $this;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $this->obtainLatOrLon($latitude);

        return $this;
    }

    public function setSpeed(string $speed): self
    {
        $this->speed = (int)$speed;

        return $this;
    }

    public function setAzimuth(string $azimuth): self
    {
        $this->azimuth = (int)$azimuth;

        return $this;
    }

    public function setTripStartInMinutes(string $tripStartInMinutes): self
    {
        $this->tripStartInMinutes = (int)$tripStartInMinutes;

        return $this;
    }

    public function setDiviationInSeconds(string $diviationInSeconds): self
    {
        $this->diviationInSeconds = (int)$diviationInSeconds;

        return $this;
    }

    public function setMeasurementTime(string $measurementTime): self
    {
        $this->measurementTime = (int)$measurementTime;

        return $this;
    }

    public function setCarType(string $carType): self
    {
        $this->carType = $carType;

        return $this;
    }

    private function obtainLatOrLon(string $coordinate): float
    {
        if (strlen($coordinate) !== 8 || !is_numeric($coordinate)) {
            return 0.0;
        }

        $coord = $coordinate[0] . $coordinate[1] . '.' . substr($coordinate, 2);
        $coord = (float)$coord;

        return $coord;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function getTripNumber(): string
    {
        return $this->tripNumber;
    }

    public function getCarNumber(): string
    {
        return $this->carNumber;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getAzimuth(): int
    {
        return $this->azimuth;
    }

    public function getTripStartInMinutes(): int
    {
        return $this->tripStartInMinutes;
    }

    public function getDiviationInSeconds(): int
    {
        return $this->diviationInSeconds;
    }

    public function getMeasurementTime(): int
    {
        return $this->measurementTime;
    }

    public function getCarType(): int
    {
        return $this->carType;
    }
}
